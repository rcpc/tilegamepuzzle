#pragma once
#include <string>
#include <fstream>
#include <array>
#include <vector>
#include <numeric>
#include "State.h"


class Game
{
public:
	Game();
	Game(const std::string& fileName);


	~Game() = default;
public:
	bool isSolvable();
	int getNumberOfInversion() const;
	void readFromFile(const std::string& fileName);
public:
	State state;
};

