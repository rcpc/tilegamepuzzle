#include "Node.h"


bool Node::equals(const Node& other)
{
	return other.state.getConfig() == this->state.getConfig();
}

Node::Node(const State& config, Node* parent)
	: state(config), Parent(parent)
{}

Node::Node(const Node & other)
	: Parent(other.Parent), state(other.state), currentDirection(other.currentDirection)
{}

bool Node::operator==(const Node& second)
{
	return this->state == second.state;
}

Node* Node::GetSuccesor()
{
	State newState(this->state);
	auto config = newState.getConfig();
	auto cursor = newState.getCursor();

	int column = (cursor % 3);
	int row = (cursor / 3);

	switch (currentDirection)
	{
	case Direction::Up:

		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (row > 0)
		{
			std::swap(config[cursor], config[cursor - 3]);
			newState.getConfig() = config;
			return new Node(newState, this);
		}
	case Direction::Down:
		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (row < 2)
		{
			std::swap(config[cursor], config[cursor + 3]);
			newState.getConfig() = config;
			return new Node(newState, this);
		}

	case Direction::Left:
		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (column > 0)
		{
			std::swap(config[cursor], config[cursor - 1]);
			newState.getConfig() = config;
			return new Node(newState, this);
		}
	case Direction::Right:
		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (column < 2)
		{
			std::swap(config[cursor], config[cursor + 1]);
			newState.getConfig() = config;
			return new Node(newState, this);
		}
	default:
		return nullptr;
	}
}

std::ostream& operator<<(std::ostream& os, Node& node)
{
	os << "N:\n" << node.state;
	return os;
}
