#include "State.h"

State::State(const std::array<int, 9>& config)
	: config(config)
{}

int State::getCursor()
{
	for (auto i = 0; i < config.size(); ++i)
		if (config[i] == 0)
		{
			cursor = i;
			return cursor;
		}

	return -1;
}

std::array<int, 9> & State::getConfig()
{
	return config;
}

const std::array<int, 9> & State::getConfig() const
{
	return config;
}

bool State::operator==(const State& other)
{
	return this->config == other.config;
}

std::ostream& operator<<(std::ostream& os, State& state)
{
	os << state.config;
	return os;
}
